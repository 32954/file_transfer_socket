# File transfer using sockets

Transfer a file from a client to a server using sockets in C.

# Compile

```bash
make
```

# How to use

First start the server like this:

```bash
./tcp_server "client_IPaddress" port_number
```

Then send a file to the server using the client like this:

```bash
./tcp_client "filepath" "server_IPaddress" port_number
```
If you want the server to accept any client IPaddress or don't wanna type its address use `"ANY_ADDR"` in `"client_IPaddress"`.