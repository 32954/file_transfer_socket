all : tcp_server tcp_client
.PHONY : all clean
tcp_server : tcp_server.c
	gcc -Wall tcp_server.c -o tcp_server
tcp_client : tcp_client.c
	gcc -Wall tcp_client.c -o tcp_client
clean :
	rm tcp_server tcp_client