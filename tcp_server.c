#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MAX_LINE 1024

void write_file(int client_sd, FILE *fd);
void sigint_handler(int signal);

size_t total_bytes = 0;

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        perror("Wrong input: ./tcp_server IPaddress port");
        exit(EXIT_FAILURE);
    }

    signal(SIGINT, sigint_handler);

    // Create socket object
    int server_sd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sd == -1)
    {
        perror("Could not create socket");
        exit(EXIT_FAILURE);
    }

    // Prepare the address
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(atoi(argv[2]));
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);

    // Bind the socket file
    if (bind(server_sd, (struct sockaddr *)&server_address,
             sizeof(server_address)) == -1)
    {
        close(server_sd);
        perror("Could not bind the address");
        exit(EXIT_FAILURE);
    }

    // Prepare backlog
    if (listen(server_sd, 10) == -1)
    {
        close(server_sd);
        perror("Could not set the backlog");
        exit(EXIT_FAILURE);
    }

    // Start accepting clients
    while (1)
    {
        struct sockaddr_in client_addr;
        socklen_t client_addr_len = sizeof(client_addr);

        int client_sd = accept(server_sd,
                               (struct sockaddr *)&client_addr, &client_addr_len);

        if (client_sd == -1)
        {
            close(server_sd);
            perror("Could not accept the client");
            exit(EXIT_FAILURE);
        }

        char client_address[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &client_addr.sin_addr, client_address, INET_ADDRSTRLEN);

        if (strcmp("ANY_ADDR", argv[1]) != 0)
        {
            if (strcmp(client_address, argv[1]) != 0)
            {
                close(client_sd);
                printf("Request from address %s not %s\n", client_address, argv[1]);
                printf("Request denied, waiting for %s\n", argv[1]);
                continue;
            }
        }

        char filename[FILENAME_MAX] = {0};
        if (recv(client_sd, filename, FILENAME_MAX, 0) == -1)
        {
            close(server_sd);
            close(client_sd);
            perror("Could not receive file name from client");
            exit(EXIT_FAILURE);
        }

        FILE *fd = fopen(filename, "wb");
        if (fd == NULL)
        {
            close(server_sd);
            close(client_sd);
            perror("Could not open file");
            exit(EXIT_FAILURE);
        }

        printf("Receiving %s from %s\n", filename, client_address);

        write_file(client_sd, fd);
        printf("Received %s of size %ld Bytes\n", filename, total_bytes);

        total_bytes = 0;

        close(client_sd);
        fclose(fd);
    }

    return 0;
}

void write_file(int client_sd, FILE *fd)
{
    size_t total;
    char buff[MAX_LINE] = {0};

    while ((total = recv(client_sd, buff, MAX_LINE, 0)) > 0)
    {
        total_bytes += total;
        if (total == -1)
        {
            close(client_sd);
            perror("Could not receive file");
            exit(EXIT_FAILURE);
        }

        if (fwrite(buff, sizeof(char), total, fd) != total)
        {
            close(client_sd);
            perror("Write file error");
            exit(EXIT_FAILURE);
        }
        memset(buff, 0, MAX_LINE);
    }
}

void sigint_handler(int signal)
{
    printf("\nServer Stopped!\n");
    exit(EXIT_SUCCESS);
}