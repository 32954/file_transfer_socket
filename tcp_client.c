#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MAX_LINE 1000

void send_file(FILE *fd, int server_sd);

size_t total_bytes = 0;

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        perror("Wrong input: ./tcp_client filepath IPaddress port");
        exit(EXIT_FAILURE);
    }

    // Create socket object
    int server_sd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sd == -1)
    {
        perror("Could not create socket");
        exit(EXIT_FAILURE);
    }

    // Prepare the address
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(atoi(argv[3]));
    if (inet_pton(AF_INET, argv[2], &server_address.sin_addr) < 0)
    {
        perror("inet_pton IPaddress conversion error");
        exit(EXIT_FAILURE);
    }

    // Connect to server
    if (connect(server_sd,
                (struct sockaddr *)&server_address, sizeof(server_address)) == -1)
    {
        close(server_sd);
        perror("Could not connect");
        exit(EXIT_FAILURE);
    }

    char *filename = basename(argv[1]);
    if (filename == NULL)
    {
        perror("basename file name conversion error");
        exit(EXIT_FAILURE);
    }

    char buff[FILENAME_MAX] = {0};
    strncpy(buff, filename, strlen(filename));
    if (send(server_sd, buff, FILENAME_MAX, 0) == -1)
    {
        perror("Could not send file name to server");
        exit(EXIT_FAILURE);
    }

    FILE *fd = fopen(argv[1], "rb");
    if (fd == NULL)
    {
        perror("Could not open file");
        exit(EXIT_FAILURE);
    }

    send_file(fd, server_sd);
    printf("Sent %s of size %ld Bytes\n", filename, total_bytes);
    fclose(fd);
    close(server_sd);

    return 0;
}

void send_file(FILE *fd, int server_sd)
{
    int total;
    char send_line[MAX_LINE] = {0};

    while ((total = fread(send_line, sizeof(char), MAX_LINE, fd)) > 0)
    {
        total_bytes += total;
        if (total != MAX_LINE && ferror(fd))
        {
            perror("Read file error");
            exit(EXIT_FAILURE);
        }

        if (send(server_sd, send_line, total, 0) == -1)
        {
            perror("Could not send file");
            exit(EXIT_FAILURE);
        }
        memset(send_line, 0, MAX_LINE);
    }
}
